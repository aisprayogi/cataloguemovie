package com.aisprayogi.listmoviefavorite.data.db;


import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class MovieDatabaseContract {
    public static final String TABLE_NAME_FAVORITES = "movie";

    // Authority yang digunakan
    public static final String AUTHORITY = "com.aisprayogi.cataloguemovie";
    public static final String SCHEME = "content";

    public static final class MovieDatabaseColumns implements BaseColumns {

        public static final String MOVIE_ID = "movie_id";
        public static final String MOVIE_TITLE = "movie_title";
        public static final String MOVIE_OVERVIEW = "movie_overview";
        public static final String MOVIE_POSTERPATH = "movie_poster_path";
        public static final String MOVIE_RELEASEDATE = "movie_release_date";
        public static final String MOVIE_FULL_OVERVIEW = "movie_full_overview";

        // Base content yang digunakan untuk akses content provider
        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME_FAVORITES)
                .build();

        public static String getColumnString(Cursor cursor, String columnName) {
            return cursor.getString( cursor.getColumnIndex(columnName) );
        }
        public static int getColumnInt(Cursor cursor, String columnName) {
            return cursor.getInt( cursor.getColumnIndex(columnName) );
        }
        public static long getColumnLong(Cursor cursor, String columnName) {
            return cursor.getLong( cursor.getColumnIndex(columnName) );
        }

    }
}
