package com.aisprayogi.listmoviefavorite.ui;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;

import com.aisprayogi.listmoviefavorite.R;
import com.aisprayogi.listmoviefavorite.data.adapter.CardViewMoviesAdapter;

import static com.aisprayogi.listmoviefavorite.data.db.MovieDatabaseContract.MovieDatabaseColumns.CONTENT_URI;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    private CardViewMoviesAdapter adapter;
    RecyclerView recyclerView;
    ProgressBar loadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle("List Movie Favorite");
        }

        recyclerView = (RecyclerView) findViewById(R.id.rv_movies);
        loadingProgressBar = findViewById(R.id.progressBar);

        adapter = new CardViewMoviesAdapter(MainActivity.this);
        Log.d("Favorite", "onCreate: adapter created");
        //adapter.notifyDataSetChanged();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        getSupportLoaderManager().initLoader(110,null,this);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(this,CONTENT_URI,null,null,null,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        adapter.setData(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        adapter.clearData();
    }
}
