package com.aisprayogi.cataloguemovie.ui.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.ui.main.UpcomingFragment;
import com.aisprayogi.cataloguemovie.ui.main.NowPlayingFragment;

public class MovieFragmentPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;
    private String tabTitles[];
    private Context context;

    public MovieFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        tabTitles = new String[] {context.getResources().getString(R.string.upcoming),
                context.getResources().getString(R.string.nowplaying),context.getResources().getString(R.string.favorite)};
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new UpcomingFragment();
                break;
            case 1:
                fragment = new NowPlayingFragment();
                break;
            case 2:
                fragment = new FavsFragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
