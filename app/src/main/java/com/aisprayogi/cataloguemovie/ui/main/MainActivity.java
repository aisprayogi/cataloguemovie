package com.aisprayogi.cataloguemovie.ui.main;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;

import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.ui.search.SearchResultActivity;
import com.aisprayogi.cataloguemovie.data.adapter.CardViewMoviesAdapter;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private CardViewMoviesAdapter adapter;
    private ArrayList<MovieItemsModel> list;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ViewPager viewPager;
    private SearchView searchView;
    private DrawerLayout drawerLayout;

    static final String EXTRAS_QUERY = "EXTRAS_QUERY";
    static final String EXTRAS_NOWPLAYING= "EXTRAS_NOWPLAYINGFLAG";
    RecyclerView recyclerView;
    boolean isNowPlaying;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MovieFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));
        viewPager.setOffscreenPageLimit(2);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setCurrentItem(0);
        String queryCari = "";
        isNowPlaying=false;

    }



    View.OnClickListener myListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Log.d("MAIN", "onResume: ");
//        if(mShimmerViewContainer.getVisibility()==View.VISIBLE)
//            mShimmerViewContainer.startShimmer();
    }



    @Override
    public void onPause() {
//        mShimmerViewContainer.stopShimmer();
        super.onPause();
        Log.d("MAIN", "onPause: ");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this, SearchResultActivity.class)));
        searchView.setQueryHint(getResources().getString(R.string.searchMovies));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        drawerLayout.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_upcoming) {
            viewPager.setCurrentItem(0);
        }

        if (id == R.id.nav_nowplaying) {
            viewPager.setCurrentItem(1);
        }

        if (id == R.id.nav_search) {
            searchView.setIconified(false);
        }

        if (id == R.id.nav_favorite) {
            viewPager.setCurrentItem(2);
        }
        return true;
    }
}