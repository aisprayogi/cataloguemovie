package com.aisprayogi.cataloguemovie.ui.detail;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;


import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract;
import com.aisprayogi.cataloguemovie.data.db.MovieDatabaseHelper;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.aisprayogi.cataloguemovie.ui.main.MainActivity;
import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;

import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.CONTENT_URI;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.MOVIE_ID;

public class MovieDetailActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "extra_person";
    boolean isFavorited = false;

    private MovieItemsModel movieItemsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        TextView detailOverview = findViewById(R.id.detail_overview);
        TextView detailReleaseDate = findViewById(R.id.detail_release_date);
        TextView detailTitle = findViewById(R.id.detail_title);
        ImageView imagePoster = findViewById(R.id.poster_film_detail);
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        View background = findViewById(R.id.movie_background);
        FloatingActionButton btnFavorite = findViewById(R.id.fab);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        movieItemsModel = getIntent().getParcelableExtra(EXTRA_MOVIE);

        collapsingToolbarLayout.setTitleEnabled(true);
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(this, R.color.colorPrimary));
        collapsingToolbarLayout.setTitle(movieItemsModel.getTitle());
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this,android.R.color.transparent));

        detailTitle.setText(movieItemsModel.getTitle());
        detailReleaseDate.setText(movieItemsModel.getReleaseDate());
        detailOverview.setText(movieItemsModel.getFullOverview());

        String whereClause = String.format("%s=?", MOVIE_ID);
        String[] args = new String[]{String.valueOf(movieItemsModel.getMovieId())};
        Cursor cursor = getContentResolver().query(CONTENT_URI,null,whereClause,args,null);
        if(cursor != null){
            if(cursor.getCount() > 0 ) {
                isFavorited = true;
                btnFavorite.setImageResource(R.drawable.ic_favorite);
            }
        }else{
            isFavorited = false;
            btnFavorite.setImageResource(R.drawable.ic_favorite_border);
        }

        final String posterURL = "http://image.tmdb.org/t/p/w300/"+movieItemsModel.getPosterPath();
        //noinspection SpellCheckingInspection
        Log.d("CATMOVIES", "onCreateDetail: "+posterURL);

        Glide
                .with(this)
                .load(posterURL)
                .listener(GlidePalette.with(posterURL)
                        .use(GlidePalette.Profile.VIBRANT)
                        .intoBackground(background,GlidePalette.Swatch.RGB)
                        .intoTextColor(detailTitle,GlidePalette.Swatch.BODY_TEXT_COLOR)
                        .crossfade(true)
                        .use(GlidePalette.Profile.MUTED_DARK)
                        .intoBackground(btnFavorite,GlidePalette.Swatch.RGB)
                        .crossfade(true))
                .into(imagePoster);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public void onFavoriteClick(View view) {
        FloatingActionButton btnFavorite = findViewById(R.id.fab);
        String whereClause = String.format("%s=?", MOVIE_ID);
        String[] args = new String[]{String.valueOf(movieItemsModel.getMovieId())};
        if(isFavorited){
            isFavorited = false;
            btnFavorite.setImageResource(R.drawable.ic_favorite_border);
            getContentResolver().delete(CONTENT_URI,whereClause,args);
        }else{
            isFavorited= true;
            btnFavorite.setImageResource(R.drawable.ic_favorite);
            MovieDatabaseHelper movieDatabaseHelper = new MovieDatabaseHelper(MovieDetailActivity.this);
            ContentValues contentValues = movieDatabaseHelper.setMovieContentValues(movieItemsModel);
            Uri uri = getContentResolver().insert(CONTENT_URI,contentValues);
        }
    }
}