package com.aisprayogi.cataloguemovie.ui.main;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.aisprayogi.cataloguemovie.data.loader.MyAsyncTaskLoader;
import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.data.adapter.CardViewMoviesAdapter;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NowPlayingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NowPlayingFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<MovieItemsModel>>{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String BUNDLE_RECYCLER_LAYOUT_NOWPLAYING = "nowplayingfragment.recycler.layout";
    private static final String BUNDLE_DATA_NOWPLAYING = "nowplayingfragment.data";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private MainActivity activity;
    private CardViewMoviesAdapter adapter;

    static final String EXTRAS_QUERY = "EXTRAS_QUERY";
    static final String EXTRAS_NOWPLAYING= "EXTRAS_NOWPLAYINGFLAG";
    RecyclerView recyclerView;
    ProgressBar loadingProgressBar;

    private NowPlayingFragment.OnFragmentInteractionListener mListener;

    public NowPlayingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpcomingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NowPlayingFragment newInstance(String param1, String param2) {
        NowPlayingFragment fragment = new NowPlayingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_movies);
        loadingProgressBar = view.findViewById(R.id.progressBar);

        adapter = new CardViewMoviesAdapter(getContext());
        Log.d("UpcomingFragment", "onCreate: adapter created");
        //adapter.notifyDataSetChanged();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LoaderManager loaderManager = getLoaderManager();
        Loader<ArrayList<MovieItemsModel>> loader = loaderManager.getLoader(0);


        if(savedInstanceState != null)
        {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT_NOWPLAYING);
            recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);

            adapter.setData(savedInstanceState.<MovieItemsModel>getParcelableArrayList(BUNDLE_DATA_NOWPLAYING));
            Log.d("NowPlaying", "onCreateView: savedinstance not null");
        }else {
            Log.d("NowPlaying", "onCreateView: savedinstance null");
            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_QUERY, "");
            bundle.putBoolean(EXTRAS_NOWPLAYING, true);

            recyclerView.setVisibility(View.GONE);
            loadingProgressBar.setVisibility(View.VISIBLE);

            if (loader == null) {
                loaderManager.initLoader(0, bundle, this);
            } else {
                loaderManager.restartLoader(0, bundle, this);
                //recyclerView.smoothScrollToPosition(0);
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            MainActivity activity = (MainActivity) context;
            this.activity = activity;
            //activity.onFragmentAttached();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @NonNull
    @Override
    public android.support.v4.content.Loader<ArrayList<MovieItemsModel>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.d("Mainfragment", "onCreateLoader: ");
        String queryCari = "";
        boolean nowPlayingFlag=false;
        if (args != null){
            queryCari = args.getString(EXTRAS_QUERY);
            nowPlayingFlag = args.getBoolean(EXTRAS_NOWPLAYING);
        }
        return new MyAsyncTaskLoader(getActivity(),queryCari,nowPlayingFlag);
    }

    @Override
    public void onLoadFinished(@NonNull android.support.v4.content.Loader<ArrayList<MovieItemsModel>> loader, ArrayList<MovieItemsModel> data) {
        recyclerView.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.GONE);
        adapter.setData(data);
    }

    @Override
    public void onLoaderReset(@NonNull android.support.v4.content.Loader<ArrayList<MovieItemsModel>> loader) {
        adapter.clearData();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT_NOWPLAYING, recyclerView.getLayoutManager().onSaveInstanceState());
        outState.putParcelableArrayList(BUNDLE_DATA_NOWPLAYING,adapter.getmData());
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
