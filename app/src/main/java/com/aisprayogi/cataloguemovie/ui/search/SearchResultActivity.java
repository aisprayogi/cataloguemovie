package com.aisprayogi.cataloguemovie.ui.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.data.loader.MyAsyncTaskLoader;
import com.aisprayogi.cataloguemovie.data.adapter.CardViewMoviesAdapter;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.aisprayogi.cataloguemovie.ui.main.MainActivity;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<MovieItemsModel>>{

    private String mParam1;
    private String mParam2;
    private MainActivity activity;
    private CardViewMoviesAdapter adapter;
    private ArrayList<MovieItemsModel> list;
    private ProgressBar loadingProgressBar;
    RecyclerView recyclerView;
    Toolbar toolbar;
    static final String EXTRAS_QUERY = "EXTRAS_QUERY";
    static final String EXTRAS_NOWPLAYING= "EXTRAS_NOWPLAYINGFLAG";
    private static final String BUNDLE_RECYCLER_LAYOUT = "search.recycler.layout";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(getString(R.string.search_results));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = (RecyclerView) findViewById(R.id.rv_movies);
        loadingProgressBar = findViewById(R.id.progressBar);


        adapter = new CardViewMoviesAdapter(SearchResultActivity.this);
        Log.d("UpcomingFragment", "onCreate: adapter created");
        //adapter.notifyDataSetChanged();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchResultActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<ArrayList<MovieItemsModel>> loader = loaderManager.getLoader(0);

        if(savedInstanceState != null)
        {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);

            adapter.setData(savedInstanceState.<MovieItemsModel>getParcelableArrayList("mdata"));
            Log.d("NowPlaying", "onCreateView: savedinstance not null");
        }else {
            if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
                String query = getIntent().getStringExtra(SearchManager.QUERY);
                Bundle bundle = new Bundle();
                bundle.putString(EXTRAS_QUERY,query);
                bundle.putBoolean(EXTRAS_NOWPLAYING,false);

                loadingProgressBar.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

                if(loader==null){
                    loaderManager.initLoader(0,bundle,this);
                }else {
                    loaderManager.restartLoader(0, bundle, this);
                    //recyclerView.smoothScrollToPosition(0);
                }
            }
        }

    }

    @NonNull
    @Override
    public Loader<ArrayList<MovieItemsModel>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.d("SearchAct", "onCreateLoader: ");
        String queryCari = "";
        boolean nowPlayingFlag=false;
        if (args != null){
            queryCari = args.getString(EXTRAS_QUERY);
            nowPlayingFlag = args.getBoolean(EXTRAS_NOWPLAYING);
        }
        return new MyAsyncTaskLoader(SearchResultActivity.this,queryCari,nowPlayingFlag);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MovieItemsModel>> loader, ArrayList<MovieItemsModel> data) {
        loadingProgressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.setData(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MovieItemsModel>> loader) {
        adapter.clearData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, recyclerView.getLayoutManager().onSaveInstanceState());
        outState.putParcelableArrayList("mdata",adapter.getmData());
    }
}
