package com.aisprayogi.cataloguemovie.ui.main;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.data.adapter.CardViewMoviesAdapter;

import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.CONTENT_URI;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{
    private CardViewMoviesAdapter adapter;
    RecyclerView recyclerView;
    ProgressBar loadingProgressBar;

    public FavsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_movies);
        loadingProgressBar = view.findViewById(R.id.progressBar);

        adapter = new CardViewMoviesAdapter(getContext());
        Log.d("Favorite", "onCreate: adapter created");
        //adapter.notifyDataSetChanged();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LoaderManager loaderManager = getLoaderManager();
        Loader<Cursor> loader = loaderManager.getLoader(110);

        recyclerView.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.VISIBLE);

        if (loader == null) {
            loaderManager.initLoader(110, null, this);
        } else {
            loaderManager.restartLoader(110, null, this);
        }
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        CursorLoader mCursorLoader = null;
        mCursorLoader = new CursorLoader(getContext(),CONTENT_URI,null,null,null,null);
        return mCursorLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        recyclerView.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.GONE);
        adapter.setData(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        adapter.clearData();
    }
}
