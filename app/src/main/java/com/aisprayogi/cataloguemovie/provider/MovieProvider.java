package com.aisprayogi.cataloguemovie.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.aisprayogi.cataloguemovie.data.db.MovieDatabaseHelper;

import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.*;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.*;



public class MovieProvider extends ContentProvider {

    /*
    Integer digunakan sebagai identifier antara select all sama select by id
     */
    private static final int MOVIE = 1;
    private static final int MOVIE_ID = 2;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        // content://com.aisprayogi.cataloguemovie/movie
        sUriMatcher.addURI(AUTHORITY, TABLE_NAME_FAVORITES, MOVIE);

        // content://com.aisprayogi.cataloguemovie/movie/id
        sUriMatcher.addURI(AUTHORITY,
                TABLE_NAME_FAVORITES + "/#",
                MOVIE_ID);
    }

    private MovieDatabaseHelper movieDatabaseHelper;

    public MovieDatabaseHelper getMovieDatabaseHelper() {
        return movieDatabaseHelper;
    }

    public void setMovieDatabaseHelper(MovieDatabaseHelper movieDatabaseHelper) {
        this.movieDatabaseHelper = movieDatabaseHelper;
    }

    @Override
    public boolean onCreate() {
        movieDatabaseHelper = new MovieDatabaseHelper(getContext());
        movieDatabaseHelper.open();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {
            case MOVIE:
                cursor = movieDatabaseHelper.queryProvider(projection,selection,selectionArgs);
                break;
            case MOVIE_ID:
                cursor = movieDatabaseHelper.queryByIdProvider(uri.getLastPathSegment());
                break;
            default:
                cursor = null;
                break;
        }

        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long added;

        switch (sUriMatcher.match(uri)) {
            case MOVIE:
                added = movieDatabaseHelper.insertProvider(values);
                break;
            default:
                added = 0;
                break;
        }

        if (added > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return Uri.parse(CONTENT_URI + "/" + added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int deleted;
        switch (sUriMatcher.match(uri)) {
            case MOVIE:
                if(selection!=null){
                    deleted = movieDatabaseHelper.deleteProvider(selection,selectionArgs);
                }else{
                    deleted =0;
                }
                break;
            case MOVIE_ID:
                deleted = movieDatabaseHelper.deleteProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }

        if (deleted > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int updated;
        switch (sUriMatcher.match(uri)) {
            case MOVIE_ID:
                updated = movieDatabaseHelper.updateProvider(uri.getLastPathSegment(), values);
                break;
            default:
                updated = 0;
                break;
        }

        if (updated > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updated;
    }
}
