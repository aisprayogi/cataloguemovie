package com.aisprayogi.cataloguemovie.data.loader;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.aisprayogi.cataloguemovie.BuildConfig;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MyAsyncTaskLoader extends AsyncTaskLoader<ArrayList<MovieItemsModel>> {

    private ArrayList<MovieItemsModel> mData;
    private boolean mHasResult = false;

    private String searchQuery;
    private boolean isNowPlaying;

    public MyAsyncTaskLoader(Context context, String searchQuery, boolean isNowPlaying) {
        super(context);

        onContentChanged();
        this.searchQuery = searchQuery;
        this.isNowPlaying = isNowPlaying;
        Log.d("ASY", "MyAsyncTaskLoader: "+searchQuery);
    }

    @Override
    protected void onStartLoading(){
        if(takeContentChanged()) {
            Log.d("ASY", "onStartLoading: just load");
            forceLoad();
        }
        else if(mHasResult) {
            Log.d("ASY", "onStartLoading: just result");
            deliverResult(mData);
        }
        Log.d("ASY", "onStartLoading: dontknow");
    }

    @Override
    public void deliverResult (final ArrayList<MovieItemsModel> data){
        Log.d("ASY", "deliverResult: ");
        mData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset(){
        super.onReset();
        onStopLoading();
        if(mHasResult){
            onReleaseResourceData(mData);
            mData = null;
            mHasResult = false;

        }
        Log.d("ASY", "onReset: ");
    }

    private void onReleaseResourceData(ArrayList<MovieItemsModel> mData) {
        Log.d("ASY", "onReleaseResourceData: ");
    }

    @Override
    public ArrayList<MovieItemsModel> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<MovieItemsModel> movieItemses = new ArrayList<>();
        String nowPlayingUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key="+ BuildConfig.API_KEY+"&language=en-US";
        String upcomingUrl = "https://api.themoviedb.org/3/movie/upcoming?api_key="+BuildConfig.API_KEY+"&language=en-US";
        String searchUrl ="https://api.themoviedb.org/3/search/movie?api_key="+
                BuildConfig.API_KEY+"&language=en-US&query="+searchQuery;
        String url = "";
        if(TextUtils.isEmpty(searchQuery)){
            if(isNowPlaying)
                url = nowPlayingUrl;
            else
                url = upcomingUrl;
        }else{
            url = searchUrl;
        }
        Log.d("CATMOVIES", "loadInBackground: "+url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String object = new String (responseBody);
                    Log.d("LOADER", "onSuccess: "+object);
                    JSONObject responseObject = new JSONObject(object);
                    JSONArray results = responseObject.getJSONArray("results");

                    for(int i=0; i < results.length(); i++){
                        JSONObject movies = results.getJSONObject(i);
                        MovieItemsModel movieItems = new MovieItemsModel(movies);
                        movieItemses.add(movieItems);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }



            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //do nothing
            }
        });

        return movieItemses;
    }
}
