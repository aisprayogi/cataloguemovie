package com.aisprayogi.cataloguemovie.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aisprayogi.cataloguemovie.data.helper.CustomOnItemClickListener;
import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;
import com.aisprayogi.cataloguemovie.ui.detail.MovieDetailActivity;
import com.aisprayogi.cataloguemovie.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CardViewMoviesAdapter extends RecyclerView.Adapter<CardViewMoviesAdapter.CardViewHolder>{
    private ArrayList<MovieItemsModel> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public CardViewMoviesAdapter (Context context){
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<MovieItemsModel> items){
        this.mData = items;
        notifyDataSetChanged();
    }

    //ambil data dari cursor
    public void setData(Cursor cursor) {
        ArrayList<MovieItemsModel> list = new ArrayList<MovieItemsModel>();
        if (cursor.moveToFirst()) {
            do {
                list.add(new MovieItemsModel(cursor));
            } while (cursor.moveToNext());
        }

        setData(list);
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_movies, parent, false);
        return new CardViewHolder(view);
    }

    private ArrayList<MovieItemsModel> getListMovies(){
        return this.mData;
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        MovieItemsModel movieItemsModel = getListMovies().get(position);

        final String posterURL = "http://image.tmdb.org/t/p/w154"+movieItemsModel.getPosterPath();
        Picasso.with(context).load(posterURL)
                .error(R.drawable.placeholder).into(holder.imagePoster);
        /*Glide.with(context)
                .load(posterURL)
                .override(350, 550)
                .into(holder.imagePoster);
                */
        holder.textViewTitle.setText(movieItemsModel.getTitle());
        holder.textViewOverview.setText(movieItemsModel.getOverview());
        holder.textViewReleaseDate.setText(movieItemsModel.getReleaseDate());

        holder.btnDetail.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                //Toast.makeText(context, "Detail "+getListMovies().get(position).getTitle(), Toast.LENGTH_SHORT).show();
                MovieItemsModel movieClicked = (MovieItemsModel) getListMovies().get(position);
                Intent movieDetailIntent = new Intent(context,MovieDetailActivity.class);
                movieDetailIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE,movieClicked);
                context.startActivity(movieDetailIntent);
            }
        }));

        holder.btnShare.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                //Toast.makeText(context, "Share "+getListMovies().get(position).getReleaseDate(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, "Judul: "+getListMovies().get(position).getTitle()+" - Tanggal release:"+getListMovies().get(position).getReleaseDate());
                    context.startActivity(intent);
            }
        }));
        Log.d("ADAPTER", "onBindViewHolder: "+holder.textViewReleaseDate.getText().toString());

    }

    @Override
    public int getItemCount() {
        return this.mData.size();
    }

    public void clearData(){
        this.mData.clear();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewOverview;
        TextView textViewReleaseDate;
        ImageView imagePoster;
        Button btnDetail, btnShare;
        public CardViewHolder(View itemView) {
            super(itemView);
            imagePoster = (ImageView) itemView.findViewById(R.id.poster_film);
            textViewOverview = (TextView) itemView.findViewById(R.id.textOverview);
            textViewTitle = (TextView) itemView.findViewById(R.id.textJudul);
            textViewReleaseDate = (TextView) itemView.findViewById(R.id.textReleaseDate);
            btnDetail = (Button)itemView.findViewById(R.id.btn_view_detail);
            btnShare = (Button) itemView.findViewById(R.id.btn_set_share);
        }
    }

    public ArrayList<MovieItemsModel> getmData() {
        return mData;
    }

    public void setmData(ArrayList<MovieItemsModel> mData) {
        this.mData = mData;
    }
}
