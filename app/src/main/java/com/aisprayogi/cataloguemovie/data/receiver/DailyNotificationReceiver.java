package com.aisprayogi.cataloguemovie.data.receiver;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.aisprayogi.cataloguemovie.R;
import com.aisprayogi.cataloguemovie.ui.main.MainActivity;

import java.util.Calendar;

import static android.os.Build.*;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class DailyNotificationReceiver extends BroadcastReceiver {
    private final int DAILY_REMINDER = 100;

    int requestCode = DAILY_REMINDER;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        String title = "Daily Reminder";
        int notifId =  DAILY_REMINDER;

        Log.d("ALARMRECEIVER_ON_REC",title+" "+notifId+" "+message);
        showAlarmNotification(context,title,message,notifId);
    }

    public void setDailyReminder(Context context){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DailyNotificationReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, "We miss you at Catalogue Movie");

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY,7);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        Log.d("ALARMRECEIVER TIMEXX", "DailyReminder Time: "+calendar.getTimeInMillis());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,requestCode, intent,0);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
        Toast.makeText(context,"Set Daily Reminder",Toast.LENGTH_SHORT).show();
    }

    private void showAlarmNotification(Context context, String title, String message, int notifId){

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder;

        // Tambahkan channel id, channel name , dan tingkat importance
        String channelId = "channel_01";
        CharSequence channelName = "dicoding channel";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        //when this notification is clicked and the upload is running, open the upload fragment
        Intent notificationIntent;
        notificationIntent = new Intent(context,MainActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);


        if(VERSION.SDK_INT < VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_movie_filter_black_24dp)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);
        }else{
            builder = new NotificationCompat.Builder(context,channelId)
                    .setSmallIcon(R.drawable.ic_movie_filter_black_24dp)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

            notificationManagerCompat.createNotificationChannel(mChannel);
        }
        builder.build().flags|= Notification.FLAG_AUTO_CANCEL;
        PendingIntent pendingIntent = PendingIntent.getActivity(context,1,notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        notificationManagerCompat.notify(notifId, builder.build());
        Log.d("loaderManager", "showAlarmNotification: ");
    }

    public void cancelAlarm(Context context){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DailyNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.cancel(pendingIntent);

        Toast.makeText(context, "Daily Reminder dibatalkan", Toast.LENGTH_SHORT).show();
    }
}
