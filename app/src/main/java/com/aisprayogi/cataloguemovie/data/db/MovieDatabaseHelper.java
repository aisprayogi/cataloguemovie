package com.aisprayogi.cataloguemovie.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.aisprayogi.cataloguemovie.data.model.MovieItemsModel;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.*;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.*;

public class MovieDatabaseHelper {
    private static String DATABASE_TABLE = TABLE_NAME_FAVORITES;
    private Context context;
    private DatabaseHelper dataBaseHelper;

    private SQLiteDatabase database;

    public MovieDatabaseHelper(Context context) {
        this.context = context;
    }

    public MovieDatabaseHelper open() throws SQLException {
        dataBaseHelper = new DatabaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }

    public void setDatabase(SQLiteDatabase database) {
        this.database = database;
    }

    public void close() {
        dataBaseHelper.close();
    }

    public ArrayList<MovieItemsModel> query() {
        ArrayList<MovieItemsModel> arrayList = new ArrayList<MovieItemsModel>();
        Cursor cursor = database.query(DATABASE_TABLE
                , null
                , null
                , null
                , null
                , null, _ID + " DESC"
                , null);
        cursor.moveToFirst();
        MovieItemsModel movie;
        if (cursor.getCount() > 0) {
            do {

                movie = new MovieItemsModel(cursor);
                arrayList.add(movie);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert(MovieItemsModel movie) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(MOVIE_ID, movie.getMovieId());
        initialValues.put(MOVIE_TITLE, movie.getTitle());
        initialValues.put(MOVIE_OVERVIEW, movie.getOverview());
        initialValues.put(MOVIE_FULL_OVERVIEW, movie.getFullOverview());
        initialValues.put(MOVIE_POSTERPATH, movie.getPosterPath());
        initialValues.put(MOVIE_RELEASEDATE, movie.getReleaseDate());
        return database.insert(DATABASE_TABLE, null, initialValues);
    }

    public int update(MovieItemsModel movie) {
        ContentValues args = new ContentValues();
        args.put(MOVIE_ID, movie.getMovieId());
        args.put(MOVIE_TITLE, movie.getTitle());
        args.put(MOVIE_OVERVIEW, movie.getOverview());
        args.put(MOVIE_FULL_OVERVIEW, movie.getFullOverview());
        args.put(MOVIE_POSTERPATH, movie.getPosterPath());
        args.put(MOVIE_RELEASEDATE, movie.getReleaseDate());
        return database.update(DATABASE_TABLE, args, _ID + "= '" + movie.getId() + "'", null);
    }

    public int delete(int id) {
        return database.delete(DATABASE_TABLE, _ID + " = '" + id + "'", null);
    }

    public Cursor queryByIdProvider(String id) {
        return database.query(DATABASE_TABLE, null
                , _ID + " = ?"
                , new String[]{id}
                , null
                , null
                , null
                , null);
    }

    public Cursor queryByMovieIdProvider(String MovieId) {
        return database.query(DATABASE_TABLE, null
                , MOVIE_ID + " = ?"
                , new String[]{MovieId}
                , null
                , null
                , null
                , null);
    }

    public Cursor queryProvider(@Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs) {
        return database.query(DATABASE_TABLE
                , projection
                , selection
                , selectionArgs
                , null
                , null
                , _ID + " DESC");
    }

    public Cursor queryProvider() {
        return database.query(DATABASE_TABLE
                , null
                , null
                , null
                , null
                , null
                , _ID + " DESC");
    }

    public Cursor queryByMovieIdProvider (MovieItemsModel movieItemsModel){
        String whereClause = String.format("%s=?",MOVIE_ID);
        String[] args = new String[]{String.valueOf(movieItemsModel.getMovieId())};
        Cursor cursor = context.getContentResolver()
                .query(CONTENT_URI,null,whereClause,args,null);
        return cursor;
    }

    public long insertProvider(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public int updateProvider(String id, ContentValues values) {
        return database.update(DATABASE_TABLE, values, _ID + " = ?", new String[]{id});
    }

    public int deleteProvider(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = ?", new String[]{id});
    }

    public int deleteProvider(String whereClause,String[] args) {
        return database.delete(DATABASE_TABLE, whereClause, args);
    }

    public ContentValues setMovieContentValues (MovieItemsModel movie){
        ContentValues values = new ContentValues();
        values.put(MOVIE_ID, movie.getMovieId());
        values.put(MOVIE_TITLE, movie.getTitle());
        values.put(MOVIE_OVERVIEW, movie.getOverview());
        values.put(MOVIE_POSTERPATH, movie.getPosterPath());
        values.put(MOVIE_RELEASEDATE, movie.getReleaseDate());
        values.put(MOVIE_FULL_OVERVIEW, movie.getFullOverview());
        return values;
    };

}
