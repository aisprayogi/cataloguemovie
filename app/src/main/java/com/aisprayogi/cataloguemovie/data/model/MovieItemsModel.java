package com.aisprayogi.cataloguemovie.data.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.*;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.*;


public class MovieItemsModel implements Parcelable {
    public static final int ID_DEFAULT_DB = 0;

    private int id;
    private int movieId;
    private String title;
    private String overview;
    private String posterPath;
    private String releaseDate;
    private String fullOverview;

    public MovieItemsModel (JSONObject object){
        try{
            //id ini digunakan untuk pengambilan item database
            this.id=ID_DEFAULT_DB;

            this.movieId = object.getInt("id");
            this.title = object.getString("title");
            this.fullOverview = object.getString("overview");
            String shortOverview;
            if(fullOverview.length()>50)
                shortOverview= this.fullOverview.substring(0,50)+"...";
            else
                shortOverview = this.fullOverview;
            this.overview = object.getString("overview");
            this.posterPath = object.getString("poster_path");

            String releaseDateOriginal = object.getString("release_date");
            //Date date = new SimpleDateFormat("EEEEEE,MMM d, yyyy").parse(releaseDateOriginal);
            this.releaseDate = formatDate(releaseDateOriginal);
            Log.d("CatMovies", "MovieItemsModel: "+getMovieId());
            Log.d("CatMovies", "MovieItemsModel: "+getOverview());
            Log.d("CatMovies", "MovieItemsModel: "+getTitle());
            Log.d("CatMovies", "MovieItemsModel: "+getReleaseDate());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public MovieItemsModel(Cursor cursor) {
        this.id = getColumnInt(cursor, _ID);
        this.movieId = getColumnInt(cursor,MOVIE_ID);
        this.title = getColumnString(cursor,MOVIE_TITLE);
        this.overview = getColumnString(cursor,MOVIE_OVERVIEW);
        this.posterPath = getColumnString(cursor,MOVIE_POSTERPATH);
        this.releaseDate = getColumnString(cursor,MOVIE_RELEASEDATE);
        this.fullOverview = getColumnString(cursor,MOVIE_FULL_OVERVIEW);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int id) {
        this.movieId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    private String formatDate (String dateInput){
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "EEE, MMM d, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try{
            date = inputFormat.parse(dateInput);
            str = outputFormat.format(date);
        }catch (Exception e){
            e.printStackTrace();
        }

        return str;
    }

    public String getFullOverview() {
        return fullOverview;
    }

    public void setFullOverview(String fullOverview) {
        this.fullOverview = fullOverview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.movieId);
        dest.writeString(this.title);
        dest.writeString(this.overview);
        dest.writeString(this.posterPath);
        dest.writeString(this.releaseDate);
        dest.writeString(this.fullOverview);
    }

    protected MovieItemsModel(Parcel in) {
        this.id = in.readInt();
        this.movieId = in.readInt();
        this.title = in.readString();
        this.overview = in.readString();
        this.posterPath = in.readString();
        this.releaseDate = in.readString();
        this.fullOverview = in.readString();
    }

    public static final Parcelable.Creator<MovieItemsModel> CREATOR = new Parcelable.Creator<MovieItemsModel>() {
        @Override
        public MovieItemsModel createFromParcel(Parcel source) {
            return new MovieItemsModel(source);
        }

        @Override
        public MovieItemsModel[] newArray(int size) {
            return new MovieItemsModel[size];
        }
    };
}
