package com.aisprayogi.cataloguemovie.data.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.*;
import static com.aisprayogi.cataloguemovie.data.db.MovieDatabaseContract.MovieDatabaseColumns.*;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME = "dbmovie";


    private static final int DATABASE_VERSION = 1;

    public static String CREATE_TABLE_MOVIE_FAVORITE = "create table "+TABLE_NAME_FAVORITES+
            " ("+ _ID+" integer primary key autoincrement, " +
            MOVIE_ID+" INTEGER not null, " +
            MOVIE_TITLE+" text not null, " +
            MOVIE_OVERVIEW+" text, " +
            MOVIE_POSTERPATH+" text, " +
            MOVIE_RELEASEDATE+" text, " +
            MOVIE_FULL_OVERVIEW+" text, "+
            "UNIQUE ("+MOVIE_ID+") ON CONFLICT REPLACE);";

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MOVIE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME_FAVORITES);
        onCreate(db);
    }

}
